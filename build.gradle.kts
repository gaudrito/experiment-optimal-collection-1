import java.io.ByteArrayOutputStream
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
import org.apache.tools.ant.taskdefs.condition.Os
import org.codehaus.groovy.ast.tools.GeneralUtils.args

plugins {
    java
    eclipse
    kotlin("jvm") version "1.3.11"
}

group = "it.unibo.alchemist"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
    maven("https://dl.bintray.com/alchemist-simulator/Alchemist/")
    maven("https://dl.bintray.com/protelis/Protelis/")
}

dependencies {
    val alchemistVersion = "9.3.0"
    api("it.unibo.alchemist:alchemist:$alchemistVersion")
    implementation("it.unibo.alchemist:alchemist-implementationbase:$alchemistVersion")
    implementation("it.unibo.alchemist:alchemist-maps:$alchemistVersion")
    implementation("it.unibo.alchemist:alchemist-incarnation-protelis:$alchemistVersion")
    implementation("org.apache.commons:commons-math3:3.6.1")
    implementation(kotlin("stdlib-jdk8"))
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_11
}
tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "11"
}

sourceSets.getByName("main") {
    resources {
        srcDirs("src/main/yaml")
        srcDirs("src/main/protelis")
    }
}

task("runTests") {
    doLast {
        println("Done.")
    }
}
fun makeTest(
    file: String,
    name: String = file,
    sampling: Double = 1.0,
    time: Double = Double.POSITIVE_INFINITY,
    vars: Set<String> = setOf(),
    maxHeap: Long? = 16000,
    taskSize: Int = 1024,
    threads: Int? = null,
    debug: Boolean = false
) {
    val heap = if(threads != null) { threads*taskSize } else { maxHeap ?: if (System.getProperty("os.name").toLowerCase().contains("linux")) {
        ByteArrayOutputStream().use { output ->
                exec {
                    executable = "bash"
                    args = listOf("-c", "cat /proc/meminfo | grep MemAvailable | grep -o '[0-9]*'")
                    standardOutput = output
                }
                output.toString().trim().toLong() / 1024
            }
            .also { println("Detected ${it}MB RAM available.") }  * 7 / 10
    } else {
        // Guess 16GB RAM of which 2 used by the OS
        14 * 1024L
    }
    }
    val threadCount = threads ?: maxOf(1, minOf(Runtime.getRuntime().availableProcessors(), heap.toInt() / taskSize ))
    println("* " + name + " running on $threadCount threads")
    task<JavaExec>("$name") {
        classpath = sourceSets["main"].runtimeClasspath
        classpath("src/main/protelis")
        main = "it.unibo.alchemist.Alchemist"
        maxHeapSize = "${heap}m"
        if (debug) {
            jvmArgs("-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=1044")
        }
        File("data/raw").mkdirs()
        args(
            "-y", "src/main/yaml/${file}.yml",
            "-t", "$time",
            "-e", "data/raw/${name}",
            "-p", threadCount,
            "-i", "$sampling"
        )
        if (vars.isNotEmpty()) {
            args("-b", "-var", *vars.toTypedArray())
            tasks {
                "runTests" {
                    dependsOn("$name")
                }
            }
        } else {
            args("-g", "src/main/resources/${file}.aes")
        }
    }
}

makeTest("isolation_test", sampling = 1.0, time = 400.0, vars = setOf("speed","hops","neigh","random"))
makeTest("isolation_test", name = "interactive")
makeTest("retain_test", sampling = 1.0, time = 400.0, vars = setOf("random","dtime","retain"))

makeTest("testall", name = "all", sampling = 1.0, time = 400.0, vars = setOf("speed","hops","neigh","dtime","retain","random"))

makeTest("isolation_retain_test", name = "zzz", sampling = 1.0, time = 400.0, vars = setOf("speed","hops","neigh","dtime","retain","random"))


makeTest("light_test", name = "le_default", sampling = 1.0, time = 400.0, vars = setOf("random"), threads = 3)
makeTest("light_test", name = "le_speed", sampling = 1.0, time = 400.0, vars = setOf("speed","random"), threads = 6)
makeTest("light_test", name = "le_hops", sampling = 1.0, time = 400.0, vars = setOf("hops","random"), threads = 6)
makeTest("light_test", name = "le_neigh", sampling = 1.0, time = 400.0, vars = setOf("neigh","random"), threads = 6)
makeTest("light_test", name = "le_dtime", sampling = 1.0, time = 400.0, vars = setOf("dtime","random"), threads = 6)
// ./gradlew le_default && ./gradlew le_speed && ./gradlew le_hops && ./gradlew le_neigh && ./gradlew le_dtime 
// ./gradlew le_default &; ./gradlew le_speed &; ./gradlew le_hops &; ./gradlew le_neigh &; ./gradlew le_dtime &;

makeTest("light_test", name = "goptc", sampling = 1.0, time = 400.0, vars = setOf("gAlgorithm","random"), threads = 9)

makeTest("isolation_test", name = "test_default", sampling = 1.0, time = 400.0, vars = setOf("random"), threads = 3)
makeTest("isolation_test", name = "test_speed", sampling = 1.0, time = 400.0, vars = setOf("speed","random"), threads = 10)
makeTest("isolation_test", name = "test_hops", sampling = 1.0, time = 400.0, vars = setOf("hops","random"), threads = 10, taskSize = 2048)
makeTest("isolation_test", name = "test_neigh", sampling = 1.0, time = 400.0, vars = setOf("neigh","random"), threads = 10, taskSize = 2048)
makeTest("isolation_test", name = "test_dtime", sampling = 1.0, time = 400.0, vars = setOf("dtime","random"), threads = 10)

makeTest("isolation_test", name = "noloss", sampling = 1.0, time = 400.0, vars = setOf("random"), threads = 10)
makeTest("isolation_test", name = "withloss", sampling = 1.0, time = 400.0, vars = setOf("usePacketLosser","packetLossR55","random"), threads = 10)


defaultTasks("runTests")
