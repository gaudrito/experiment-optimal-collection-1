#!/usr/bin/env bash

if (($# != 4)); then
  echo "USE: ./showImgs <imgDir> <prefixImages> <numColumns> <outFilename>";
else
  set -x
  mkdir -p $1/summaries
  montage -tile $3x -density 500 -quality 100 -geometry +0+0 $1/$2*.pdf $1/$4.pdf && xdg-open $1/$4.pdf
  #montage -tile $3x $1/$2*.pdf $1/$4.pdf miff:- | convert miff:- -resize 2024x2024 $1/$4.pdf
fi
